# Exploratory Data Analysis of UFJF Network Dataset

Exploratory Data Analysis (EDA) of UFJF network dataset for publication @ CCGRID 2022

## Install Requirements
```bash
sudo pip3 install virtualenv
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Credits
Università degli studi di Messina ([unime.it](https://www.unime.it/it)) and Universidade Federal de Juiz de Fora ([ufjf.br/ufjf](https://www2.ufjf.br/ufjf/)).